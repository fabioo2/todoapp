const express = require('express');
const router = express.Router();

// Item Model
const TodoItem = require('../../models/TodoItem');

//@route GET api/items

router.get('/', (req, res) => {
  TodoItem.find({}, (err, tasks) => {
    res.render('todo.ejs', { todoItems: tasks });
  });
});

//@route POST api/items
router.post('/', async (req, res) => {
  const todoItem = new TodoItem({
    content: req.body.content,
  });
  try {
    await todoItem.save();
    res.redirect('/api/todos');
  } catch (err) {
    res.redirect('/api/todos');
  }
});

//@route DELETE api/items/delete/:id

router.route('/delete/:id').get((req, res) => {
  const id = req.params.id;
  TodoItem.findByIdAndRemove(id, (err) => {
    if (err) return res.send(500, err);
    res.redirect('/api/todos/');
  });
});
module.exports = router;
