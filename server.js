const express = require('express');
const mongoose = require('mongoose');
const app = express();

const todos = require('./routes/api/todos');

app.use('/static', express.static('public'));
app.use(express.urlencoded({ extended: true }));

//body parser 
app.use(express.json());

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to Mongo
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('MongoDB Connected...'))
  .catch((err) => console.log(err));

//view engine configuration
app.set('view engine', 'ejs');

//Use routes
app.use('/api/todos', todos);

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server started on port ${port}`));
