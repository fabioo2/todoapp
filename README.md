## This is my Todo App.    

####  Things I want to do with immediately is add:    
-edit route  
-add animations    

#### Things I want to do in the near future    
-implement a front end framework like react    


## How to build install and use my project      
1. Fork Repository     
2. Install dependencies    
3. Run start script    

## Why I chose the MIT License   
This is one of the most common licenses. It is short and simple and open source friendly! The license is also very permissive and will allow other users to change the code, publish it, sell it as long as they do not remove the copyright text. 
